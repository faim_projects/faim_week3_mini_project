package mini.project.items;

public class Items {

	private int ID;
	private String name;
	private int qty;
	private double price;
	
	public Items(int ID, String name, int qty, double price) {
		super();
		this.ID = ID;
		this.name = name;
		this.qty = qty;
		this.price = price;
	}
	
	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}


	@Override
	public String toString() {
		return "Items [ID=" + ID + ", name=" + name + ", qty=" + qty + ", price=" + price + "]";
	}

}