package mini.project.bills;


import java.time.LocalDateTime;
import java.util.*;

import mini.project.items.Items;

public class Bills {
	
	private String name;
	private List<Items> items;
	private double price;
	private String time;
	
	public Bills() {
		
	}
	
	public Bills(String name, List<Items> items, double price, String time) throws IllegalArgumentException {
		super();
		this.name = name;
		this.items = items;
//		if(price<0){
//			throw new IllegalArgumentException("exception occured, cost cannot less than zero");
//		}
		this.price = price;
		this.time = time;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Items> getItems() {
		return items;
	}
	
	public void setItems(List<Items> selectedItems) {
		this.items = selectedItems;
	}
	
	public double getCost() {
		return price;
	}
	
	public void setCost(double price) {
		this.price = price;
	}
	
	public String getTime() {
		return time;
	}
	
	public void setTime(String date) {
		this.time = date;
	}
	

}