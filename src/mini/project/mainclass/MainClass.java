package mini.project.mainclass;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import mini.project.bills.Bills;
import mini.project.items.Items;

public class MainClass {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Date time=new Date();
		List<Bills> bills = new ArrayList<Bills>();
		boolean totalDueBill;

		List<Items> items = new ArrayList<Items>();

		Items i1 = new Items(1, "Rajma Chawal", 2, 150.0);
		Items i2 = new Items(2, "Momos", 2, 190.0);
		Items i3 = new Items(3, "Red thai Curry", 2, 180.0);
		Items i4 = new Items(4, "Chaap", 2, 190.0);
		Items i5 = new Items(5, "Chilly Potato", 1, 250.0);

		items.add(i1);
		items.add(i2);
		items.add(i3);
		items.add(i4);
		items.add(i5);

		System.out.println("Welcome to Surabi Restaurant");
		System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++");

		while (true) {
			
			System.out.println("Please Enter the Credentials");

			System.out.print("Email: ");
			String email = sc.next();

			System.out.print("Password: ");
			String password = sc.next();

			String name = "";
			for(int i=0;i<email.length();i++) {
				if(email.charAt(i) == '@') {
					break;
				}else {
					name = name + email.charAt(i);
				}
			}
			
			Bills bill = new Bills();
			List<Items> orderList = new ArrayList<Items>();
			double totalBill = 0;
			
			Date date=new Date();
			ZonedDateTime time1 = ZonedDateTime.now();
			DateTimeFormatter formattedDate = DateTimeFormatter.ofPattern("E MMM dd HH:mm:ss zzz yyyy"); 
			String currentTime = time1.format(formattedDate);
			
			//System.out.println(currentTime);
			DateTimeFormatter formattedDate1 = DateTimeFormatter.ofPattern("dd");
			String currentTime1 = time1.format(formattedDate1);
			//System.out.println(currentTime1);
			
			LocalDate n = LocalDate.now();
			LocalDate erl = n.minusMonths(0);
			String lastMonth = erl.getMonth().toString();
			//System.out.println(lastMonth);
			String tChar = "";
			for(int i=0;i<3;i++) {
				tChar = tChar + lastMonth.charAt(i);
			}
			//System.out.println(tChar.toLowerCase());
			
			
			
			System.out.println("Please Enter A if you are Admin and U if you are User");
			String typeOfPerson = sc.next();

			if (typeOfPerson.equalsIgnoreCase("u")) { 

				System.out.println("Welcome Mr " + name);
				do {
					System.out.println("Today's Menu :- ");
					System.out.println("1 Rajma Chaval 2 150.0");
					System.out.println("2 momos 2 190.0");
					System.out.println("3 Red thai Curry 2 180.0");
					System.out.println("4 chaap 2 190.0");
					System.out.println("5 chilly patato 1 250.0");

					System.out.println("Enter the Menu Item Code");
					int choice = sc.nextInt();
					
					switch (choice) {
					case 1:
						orderList.add(i1);
						totalBill = totalBill + i1.getPrice();
						break;
					case 2:
						orderList.add(i2);
						totalBill = totalBill + i2.getPrice();
						break;
					case 3:
						orderList.add(i3);
						totalBill = totalBill + i3.getPrice();
						break;
					case 4:
						orderList.add(i4);
						totalBill = totalBill + i4.getPrice();
						break;
					case 5:
						orderList.add(i5);
						totalBill = totalBill + i5.getPrice();
						break;

					default:
						System.out.println("Invalid Choice");
						break;
					}
	
					System.out.println("Press 0 to show bill");
					System.out.println("Press 1 to order more");
					
					int extra= sc.nextInt();
					
					if (extra == 1)
						totalDueBill = true;
					else
						totalDueBill = false;

				} while (totalDueBill);

				System.out.println("Thanks Mr " + name + " for dining in with Surabi ");
				System.out.println("Items you have Selected: ");
				
				orderList.stream().forEach(item -> System.out.println(item));
				
				System.out.println("Your Total bill is: " + totalBill);
				
				bill.setName(name);
				bill.setCost(totalBill);
				bill.setItems(orderList);
				bill.setTime(date.toString());
				bills.add(bill);

			} else if (typeOfPerson.equalsIgnoreCase("a")) {
				
				System.out.println("Welcome Admin");
				System.out.println("Press 1 to see all the bills for today");
				System.out.println("Press 2 to see all the bills for this month");
				System.out.println("Press 3 to see all the bills");
				
				
				int option = sc.nextInt();
				
				switch (option) {
				case 1:
					if (!bills.isEmpty()) {
						for (Bills b : bills) {
							String st = b.getTime().substring(8,10);
							if(st.equalsIgnoreCase(currentTime1)) {
								System.out.println("Username :- " + b.getName());
								System.out.println("Items :- " + b.getItems());
								System.out.println("Total :- " + b.getCost());
								System.out.println("Date :- " + b.getTime() + "\n");
							}else {
								System.out.println("Invalid input");
							}
						}
					} else {
						System.out.println("No Bills today.!");
					}
					break;

				case 2:
					if (!bills.isEmpty()) {
						for (Bills b : bills) {
							String myStr = b.getTime().substring(4,7);
							if (myStr.equalsIgnoreCase(tChar)) {
								System.out.println("pre Month");
								System.out.println("UserName :- " + b.getName());
								System.out.println("Items :- " + b.getItems());
								System.out.println("Total :- " + b.getCost());
								System.out.println("Date :- " + b.getTime());
							}else {
								System.out.println("You have Entered an Invalid input");
							}
						}
					} else {
						System.out.println("No Bills found in this month");
					}
					break;

				case 3:
					if (!bills.isEmpty()) {
						for (Bills b : bills) {
							System.out.println("UserName :- " + b.getName());
							System.out.println("Items :- " + b.getItems());
							System.out.println("Total :- " + b.getCost());
							System.out.println("Date :- " + b.getTime());
						}
					} else
						System.out.println("No Bills.!");
					break;

				default:
					System.out.println("You have Entered an Invalid Choice");
					System.exit(1);
				}
			} else if (typeOfPerson.equalsIgnoreCase("l")) { //("L") || typeOfPerson.equals("l")
				System.exit(1);
			}else  {
				System.out.println("You have Entered an Invalid Input");
			}

		}

	}
}
